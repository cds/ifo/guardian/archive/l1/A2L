# ASC guardian

from guardian import GuardState, NodeManager
import subprocess
import sys
from ezca import Ezca
from math import sqrt, pow
import time 
import subprocess
import cdsutils

sys.path.append('/ligo/cdscfg')
import stdenv as cds;

cds.INIT_ENV()

Ezca().export()

import isclib.matrices as matrix

################################################
# angle to length minimization
################################################

class INIT(GuardState):
    def main(self):
        return

class A2L(GuardState):
    request = True
    def main(self):
        return

# quad dither settings
        itmx_p_freq = 19.125 
        itmy_p_freq = 19.653
        etmx_p_freq = 20.131
        etmy_p_freq = 20.789

        itmx_y_freq = 21.287
        itmy_y_freq = 21.851
        etmx_y_freq = 22.347
        etmy_y_freq = 22.975

        quad_clk_gain = 100
        quad_sin_gain = 1
        quad_cos_gain = 1

        t_ramp = 5

# demod phases
        pit1_ph = 45
        pit2_ph = 45
        pit3_ph = 45
        pit4_ph = 45
        yaw1_ph = 45
        yaw2_ph = 45
        yaw3_ph = 45
        yaw4_ph = 45

#########################################
# Ramp down the dither lines
        print 'Turning off beam spot monitor dither lines'
        for dof in ['PIT1','PIT2','PIT3','PIT4','PIT5','YAW1','YAW2','YAW3','YAW4','YAW5']:
            ezca['ASC-ADS_' + dof + '_OSC_TRAMP'] = 5
            time.sleep(0.3)
            ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = 0
            ezca['ASC-ADS_' + dof + '_OSC_SINGAIN'] = 0
            ezca['ASC-ADS_' + dof + '_OSC_COSGAIN'] = 0
            time.sleep(6)

#########################################
# Set up matrices

#Zero all matrices first
        print 'Zeroing all matrices'
        matrix.asc_ads_lo_pit.zero()
        matrix.asc_ads_lo_yaw.zero()
        matrix.asc_ads_input.zero()
        matrix.asc_ads_output_pit.zero()
        matrix.asc_ads_output_yaw.zero()

        print 'Setting oscillator matrices'
#pit oscillator matrix
        matrix.asc_ads_lo_pit['ITMX','OSC1'] = 1
        matrix.asc_ads_lo_pit['ITMY','OSC2'] = 1
        matrix.asc_ads_lo_pit['ETMX','OSC3'] = 1
        matrix.asc_ads_lo_pit['ETMY','OSC4'] = 1

#yaw oscillator matrix
        matrix.asc_ads_lo_yaw['ITMX','OSC1'] = 1
        matrix.asc_ads_lo_yaw['ITMY','OSC2'] = 1
        matrix.asc_ads_lo_yaw['ETMX','OSC3'] = 1
        matrix.asc_ads_lo_yaw['ETMY','OSC4'] = 1

        print 'Setting demod input matrix'
        matrix.asc_ads_input['PIT1','DARM_CTRL'] = 1
        matrix.asc_ads_input['PIT2','DARM_CTRL'] = 1
        matrix.asc_ads_input['PIT3','DARM_CTRL'] = 1
        matrix.asc_ads_input['PIT4','DARM_CTRL'] = 1
        matrix.asc_ads_input['YAW1','DARM_CTRL'] = 1
        matrix.asc_ads_input['YAW2','DARM_CTRL'] = 1
        matrix.asc_ads_input['YAW3','DARM_CTRL'] = 1
        matrix.asc_ads_input['YAW4','DARM_CTRL'] = 1

#########################################
#Set up dither lines

        print 'Send the dither lines'
# quads
        ezca['ASC-ADS_PIT1_OSC_FREQ'] = itmx_p_freq
        ezca['ASC-ADS_YAW1_OSC_FREQ'] = itmx_y_freq

        ezca['ASC-ADS_PIT2_OSC_FREQ'] = itmy_p_freq
        ezca['ASC-ADS_YAW2_OSC_FREQ'] = itmy_y_freq

        ezca['ASC-ADS_PIT3_OSC_FREQ'] = etmx_p_freq
        ezca['ASC-ADS_YAW3_OSC_FREQ'] = etmx_y_freq

        ezca['ASC-ADS_PIT4_OSC_FREQ'] = etmy_p_freq
        ezca['ASC-ADS_YAW4_OSC_FREQ'] = etmy_y_freq

        for dof in ['PIT1','PIT2','PIT3','PIT4','YAW1','YAW2','YAW3','YAW4']:
            ezca['ASC-ADS_' + dof + '_OSC_TRAMP'] = t_ramp
            time.sleep(0.3)
            ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = quad_clk_gain
            ezca['ASC-ADS_' + dof + '_OSC_SINGAIN'] = quad_sin_gain
            ezca['ASC-ADS_' + dof + '_OSC_COSGAIN'] = quad_cos_gain

#########################################
# Set up demods

# Set the phases
        ezca['ASC-ADS_PIT1_DEMOD_PHASE'] = pit1_ph
        ezca['ASC-ADS_PIT2_DEMOD_PHASE'] = pit2_ph
        ezca['ASC-ADS_PIT3_DEMOD_PHASE'] = pit3_ph
        ezca['ASC-ADS_PIT4_DEMOD_PHASE'] = pit4_ph
        ezca['ASC-ADS_YAW1_DEMOD_PHASE'] = yaw1_ph
        ezca['ASC-ADS_YAW2_DEMOD_PHASE'] = yaw2_ph
        ezca['ASC-ADS_YAW3_DEMOD_PHASE'] = yaw3_ph
        ezca['ASC-ADS_YAW4_DEMOD_PHASE'] = yaw4_ph

# Choose the right BP filter
        ezca.switch('ASC-ADS_PIT1_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT2_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT3_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT4_DEMOD_SIG','FM1','OFF','FM2','ON')
       
        ezca.switch('ASC-ADS_YAW1_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW2_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW3_DEMOD_SIG','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW4_DEMOD_SIG','FM1','OFF','FM2','ON')

# Choose the right LP filter
        ezca.switch('ASC-ADS_PIT1_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT2_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT3_DEMOD_I','FM1','OFF','FM2','ON') 
        ezca.switch('ASC-ADS_PIT4_DEMOD_I','FM1','OFF','FM2','ON') 
        ezca.switch('ASC-ADS_PIT1_DEMOD_Q','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT2_DEMOD_Q','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_PIT3_DEMOD_Q','FM1','OFF','FM2','ON') 
        ezca.switch('ASC-ADS_PIT4_DEMOD_Q','FM1','OFF','FM2','ON') 
 
        ezca.switch('ASC-ADS_YAW1_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW2_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW3_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW4_DEMOD_I','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW1_DEMOD_Q','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW2_DEMOD_Q','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW3_DEMOD_Q','FM1','OFF','FM2','ON')
        ezca.switch('ASC-ADS_YAW4_DEMOD_Q','FM1','OFF','FM2','ON')


# Track the minimum: use demod_decoup code running for all frequencies at the same time
# Input arguments: optic_name pit/yaw osc_nb linFitFailOptn
        p1 = subprocess.Popen(['run_1TM_a2l.py', 'ITMX', 'PIT', '1', '2'],stdout=subprocess.PIPE)
        p2 = subprocess.Popen(['run_1TM_a2l.py', 'ITMY', 'PIT', '2', '2'],stdout=subprocess.PIPE)
        p3 = subprocess.Popen(['run_1TM_a2l.py', 'ETMX', 'PIT', '3', '2'],stdout=subprocess.PIPE)
        p4 = subprocess.Popen(['run_1TM_a2l.py', 'ETMY', 'PIT', '4', '2'],stdout=subprocess.PIPE)
        p5 = subprocess.Popen(['run_1TM_a2l.py', 'ITMX', 'YAW', '1', '2'],stdout=subprocess.PIPE)
        p6 = subprocess.Popen(['run_1TM_a2l.py', 'ITMY', 'YAW', '2', '2'],stdout=subprocess.PIPE)
        p7 = subprocess.Popen(['run_1TM_a2l.py', 'ETMX', 'YAW', '3', '2'],stdout=subprocess.PIPE)
        p8 = subprocess.Popen(['run_1TM_a2l.py', 'ETMY', 'YAW', '4', '2'],stdout=subprocess.PIPE)
        p1.communicate()
        p2.communicate()
        p3.communicate()
        p4.communicate()
        p5.communicate()
        p6.communicate()
        p7.communicate()
        p8.communicate()

#########################################
# Stop the oscillations and end the test
        subprocess.call("./stop_osc.py")

        print "a2l script done!"
